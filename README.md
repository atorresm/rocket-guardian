# Rocket Guardian

ZOMBIES ARE FALLING ALONG THE CITY! As the Rocket Guardian, do your duty and destroy them all.

Rocket Guardian is a android game about a guardian that must protect a city from the falling zombies.

## Download

<a href="https://f-droid.org/app/atm.rocketguardian">
    <img src="https://f-droid.org/badge/get-it-on.png"
         alt="Get it on F-Droid" height="80">
</a>

Current version: 1.0.1

[Releases](https://gitlab.com/atorresm/rocket-guardian/tags)

[See all my games on itch.io](https://papaya-games.itch.io/)

[Help me pay Google Play license fee](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M7YKJ3W9E9BVY)

## Building with Gradle
    ./gradlew android:assembleRelease

## License

>This program is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See LICENSE file for the full text of the license.
